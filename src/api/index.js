import axios from 'axios';

const url= 'https://covid19.mathdro.id/api/';

export const fetchData =async(country)=>{
    let changeurl=url;
    if(country && country!=='global'){
      changeurl=url+"countries/"+country;
       
    }
    
    try{
    const {data: {confirmed,recovered,deaths,lastUpdate}}=await axios.get(changeurl);
const modifiedData={
    confirmed,
    recovered,
    deaths,
    lastUpdate
}
return modifiedData;
    } catch(error){
        return "error is there";
    }
}
 export const fetchDailydata=async()=>{
    try{
        const {data}=await axios.get(`${url}daily`);
      const modifiedData=data.map((dailyData)=>({
          confirmed:dailyData.confirmed.total,
          deaths:dailyData.deaths.total,
          date:dailyData.reportDate
      }));
      return modifiedData;
    } catch(error){

    }
}

export const fetchcountries=async()=>{
    try{
  const {data:{countries}}=await axios.get(`${url}countries`);
//   console.log("count",countries);
  return countries.map((country)=>country.name);
    } catch(error){

    }
}
