import React, {useState,useEffect} from 'react';
import {fetchDailydata} from '../../api';
import { Line, Bar } from "react-chartjs-2";
import styles from './Chart.module.css';

const Chart=({country,data})=>{
    const [dailyData,setDailydata]=useState([]);
    useEffect(()=>{
        const fetchApi=async()=>{
            setDailydata(await fetchDailydata());
        }
        fetchApi();
    },[]);
//    let dataEver=dailyData;
//     if(props){
//         dailyData=props.data;
//     }
    const lineChart=(
        dailyData.length?(
        dailyData[0]?(
        <Line
        data={{
            labels:dailyData.map(({date})=>date),
            datasets:[{
                data:dailyData.map(({confirmed})=>confirmed),
                label:'Infected',
                borderColor:'#3333ff',
                fill:true
            },{
                data:dailyData.map(({deaths})=>deaths),
                label:'Deaths',
                borderColor:'red',
                bordergroundColor:'rgba(255.0.0.0.5)',
                fill:true
            }],

        }}
        />):null
     ):null );
     const barChart=(
         data.confirmed
         ?(
             <Bar
             data={{
                labels:['Infected(Total)','Recovered','Deaths'],
                datasets:[{
                  label:'People',
                  backgroundColor:['rgba(0,0,255,0.5)','rgba(0,255,0,0.5)','rgba(255,0,0,0.5)'],
                  data:[data.confirmed.value,data.recovered.value,data.deaths.value]  
                }]
             }}
             options={{
                 legend:{display:false},
                 title:{display:true,text:`Current state in ${country}`}
             }}
             />
         ) :null
     );
    return(
        <div className={styles.container}>
            {country?barChart:lineChart}
        </div>
    );
}
export default Chart;