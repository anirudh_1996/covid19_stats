import React from 'react';
import {Cards, Chart, CountryPicker} from './components';
import './App.css';
import {fetchData, fetchDailydata} from './api/index'
import image from './assets/image.png';
class App extends React.Component{
  state={
    data:{},
    country:''
  }
async componentDidMount(){
const fetcheddata=await fetchData();
// const fetchDaily =await fetchDailydata();
// console.log(fetchDaily);
this.setState({data:fetcheddata})
  }
  handleCountryChange=async (country)=>{
    if(country=='global'){
      country='';
    }
    const fetcheddata=await fetchData(country);
    this.setState({data:fetcheddata,country})

  }
render(){
  const{data,country}=this.state;
  return (
    <div className="container">
      <img className='image' src={image} alt="CoVID-19" />
      <CountryPicker handleCountryChange={this.handleCountryChange} />
      <Cards data={data}/>
      
      <Chart country={country} data={data}/>
    </div>
  );
}
}
export default App;
